// const Card = require('./components/Card')
class Card extends React.Component {
    render() {
        return (
            <div className="card">
                <div className="card-image">
                    <p className="date">{this.props.data.date}</p>
                    <img src={this.props.data.image} />
                    <p className="photos">PHOTOS</p>
                </div>
                <div className="card-details">
                    <h3>{this.props.data.title}</h3>
                    <p className="discription">{this.props.data.discription}</p>
                    <p>{this.props.data.details}</p>
                    <div className="card-bottom">
                        <p><span><b>o </b></span>{this.props.data.time} mins ago</p>
                        <img className="ikon" src={this.props.data.ikon} />
                        <p>{this.props.data.comments} comments</p>
                    </div>

                </div>

            </div>
        )
    }
}
let cardData = {
    title: "City Lights in New York",
    discription: "The city that never sleeps",
    details: "New York, the largest city in the us., is an architectural marvel with pleanty of historic monuments",
    time: 6,
    comments: 39,
    date: '27 MAR',
    ikon: './Asserts/comments-solid.svg',
    image: 'https://images.unsplash.com/photo-1556807044-eaf2e0eecb6d?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxNzY2ODF8MHwxfHNlYXJjaHw1fHxuZXd5b3JrfGVufDB8fHx8MTY3MTY4ODI1Nw&ixlib=rb-4.0.3&q=80&w=1080'
}
let body = <>
    <h1>All Articles</h1>
    <h2>Collection of best articles on startups</h2>
    <div className="whole-container">
        <Card data={cardData} />
        <Card data={cardData} />
        <Card data={cardData} />
    </div>
</>

// console.log(React.Component);



ReactDOM.render(body, document.getElementById('root'))
